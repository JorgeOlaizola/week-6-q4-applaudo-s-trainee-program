import React, { useState } from 'react';
import { Route } from 'react-router-dom';
import Landing from './pages/Landing';
import Home from './pages/Home';
import Detail from './pages/Detail';
import LogIn from './pages/LogIn';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import './styles/globals.scss';
import { Routes } from './interfaces/Enum';
import { useLocalStorage } from './hooks/useLocalStorage';
import { ISession } from './interfaces/User';
import Profile from './pages/Profile';

function App() {
  const [listener, setListener] = useState<boolean>(false);
  const [session, setSession] = useLocalStorage('session', {});

  const useListener = () => {
    setListener(!listener);
  };

  const isAuthenticated = () => {
    if (session.user) return true;
    return false;
  };

  const useSession = (session: ISession | {}) => {
    setSession(session);
  };

  return (
    <div className='root'>
      <Navbar user={session} useListener={useListener} />
      <Route exact path={Routes.landing} render={() => <Landing />} />
      <Route exact path={Routes.videogames} render={() => <Home listener={listener} />} />
      <Route
        exact
        path={Routes.login}
        render={() => (
          <LogIn
            useListener={useListener}
            isAuthenticated={isAuthenticated}
            useSession={useSession}
          />
        )}
      />
      <Route
        exact
        path={Routes.profile}
        render={() => (
          <Profile
            useListener={useListener}
            isAuthenticated={isAuthenticated}
            session={session}
            useSession={useSession}
          />
        )}
      />
      <Route
        path={Routes.videogameDetail}
        render={({ match }) => <Detail id={match.params.id} user={session} />}
      />
      <Footer />
    </div>
  );
}

export default App;
