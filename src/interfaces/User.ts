export interface IRole {
    description: string;
    id: number;
    name: string;
    type: string;
}

export interface IUser {
    blocked: boolean;
    comments: any[];
    confirmed: boolean;
    created_at: string;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
    provider: string;
    role: IRole;
    updated_at: string;
    username: string;
}

export interface ISession {
    jwt: string;
    user: IUser;
}
