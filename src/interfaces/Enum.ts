/* eslint-disable no-unused-vars */
export enum Routes {
    landing = '/',
    login = '/login',
    profile= '/profile',
    videogames = '/videogames',
    videogameDetail = '/videogames/:id'
  }
