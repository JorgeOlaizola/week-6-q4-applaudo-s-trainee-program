export interface IGenre{
    id: number;
    name: string;
}

export interface IPlatform{
    id: number;
    name: string;
}

export interface IVideogame {
    id: number;
    name: string;
    image: string;
}

export interface IComment {
    comment: string;
    user: string;
    id?: number;
  }

export interface IVideogameDetail {
    id?: string;
    name: string;
    genre: IGenre;
    releaseYear: number;
    price: string;
    image: string | null;
    platforms: IPlatform[];
}
