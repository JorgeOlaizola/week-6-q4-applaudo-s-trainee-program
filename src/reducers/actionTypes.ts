export const INIT_FETCH = 'INIT_FETCH';
export const FINISH_FETCH = 'FINISH_FETCH';
export const PAGINATE = 'PAGINATE';
export const UPDATE = 'UPDATE';
