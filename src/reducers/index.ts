import {
  INIT_FETCH, FINISH_FETCH, PAGINATE, UPDATE
} from './actionTypes';

export const reducer = (state: any, action: { type: string; payload?: any }) => {
  switch (action.type) {
    case INIT_FETCH:
      return { ...state, loading: true };
    case FINISH_FETCH:
      return { ...state, videogames: action.payload, loading: false };
    case PAGINATE:
      return { ...state, currentPage: action.payload };
    case UPDATE:
      return { ...state, interaction: action.payload };
    default:
      return state;
  }
};

export const initialState = {
  videogames: [],
  loading: false,
  currentPage: 1,
  interaction: '',
  videogamesPerPage: 8,
};
