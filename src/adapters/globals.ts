export const BASE_URL = 'https://trainee-gamerbox.herokuapp.com';
export const DEFAULT_IMG = 'https://cdn.onlinewebfonts.com/svg/img_98811.png';
export const PAGE_ICON = 'https://image.flaticon.com/icons/png/512/1223/1223316.png';
export const SERVER_ERROR_MSG = 'Servers currently not working. Please, try later!';
