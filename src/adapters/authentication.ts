import { BASE_URL } from './globals';
import { post, IResponse } from '../API/FetchInfo';

interface ILogInBody {
  identifier: string;
  password: string;
}

export function logIn(body: ILogInBody, headers: any) {
  return post<any, IResponse>(`${BASE_URL}/auth/local`, body, { headers });
}

export function logOut(): void {
  localStorage.setItem('session', JSON.stringify({}));
}
