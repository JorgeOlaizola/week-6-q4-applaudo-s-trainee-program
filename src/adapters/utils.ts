export interface ILogIn {
  identifier: string;
  password: string
}
export interface IHeaders {
  'Content-type': string;
  Authorization?: string;
}

export function AuthorizationHeader(token: string): any {
  return {
    'Content-type': 'application/json; charseft=UFT-8',
    Authorization: token ? `Bearer ${token}` : ''
  };
}

export function RegularHeader(): any {
  return {
    'Content-type': 'application/json; charseft=UFT-8'
  };
}
