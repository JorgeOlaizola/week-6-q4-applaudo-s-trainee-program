import { useRef, useEffect } from 'react';

export default function usePrevious(prevPage: string) {
  const page: any = useRef();
  useEffect(() => {
    page.current = prevPage;
  }, [prevPage]);
  return page.current;
}
