import React, { memo } from 'react';
import { useHistory } from 'react-router';
import { DEFAULT_IMG } from '../../adapters/globals';
import { IVideogame } from '../../interfaces/Videogames';

function Videogame({ name, image, id }: IVideogame) {
  const history = useHistory();
  return (
    <div onClick={() => history.push(`/videogames/${id}`)} className='videogame'>
      <h1 className='videogame-title'>{name}</h1>
      <img className='videogame-image' src={image || DEFAULT_IMG} alt={`${name}_${id}_img`}></img>
      <span className='videogame-button'>Click for more details ⤵</span>
    </div>
  );
}

export default memo(Videogame);
