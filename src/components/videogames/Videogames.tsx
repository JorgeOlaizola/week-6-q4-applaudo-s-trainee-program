import React from 'react';
import '../../styles/videogames.scss';
import Videogame from './Videogame';
import { IVideogame } from '../../interfaces/Videogames';

export default function Videogames({
  videogames,
  loading,
}: {
  videogames: IVideogame[];
  loading: boolean;
}) {
  return (
    <div className='videogames-container'>
      {loading
        ? 'Loading...'
        : videogames &&
          videogames.map(({ id, name, image }: IVideogame) => (
            <Videogame key={id} name={name} id={id} image={image}></Videogame>
          ))}
    </div>
  );
}
