import React, { FormEvent, useState } from 'react';
import '../../styles/searchbar.scss';
import { useHistory } from 'react-router';

export default function searchVideogames({
  interactionSetter,
}: {
  // eslint-disable-next-line no-unused-vars
  interactionSetter: (query: string) => void;
}) {
  const history = useHistory();
  const [search, setSearch] = useState('');

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    history.push(`?search=${search}`);
    interactionSetter(search);
    setSearch('');
  };

  return (
    <div className='searchbar-container'>
      <form onSubmit={handleSubmit}>
        <input
          className='searchbar-input'
          onChange={(event) => setSearch(event.target.value)}
          type='text'
          value={search}
          placeholder='Search videogames by title'
        ></input>
        <input type='submit' className='searchbar-button' />
      </form>
    </div>
  );
}
