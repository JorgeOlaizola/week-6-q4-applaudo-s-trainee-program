import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import '../../styles/navbar.scss';
import { PAGE_ICON } from '../../adapters/globals';
import User from './User';
import { ISession } from '../../interfaces/User';

function Navbar({ user, useListener }: { user: ISession, useListener: () => void }) {
  return (
    <nav>
      <Link to='/'>
      <img className='icon' alt='page-icon' src={PAGE_ICON} />
      </Link>
      <Link onClick={() => useListener()} className='nav-item' to='/videogames'>
        Videogames
      </Link>
      <Link to='/login'>
      <User user={user} />
      </Link>
    </nav>
  );
}

export default memo(Navbar);
