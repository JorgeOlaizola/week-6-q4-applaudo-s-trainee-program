import React from 'react';
import { ISession } from '../../interfaces/User';
import '../../styles/user.scss';

export default function User({ user }: { user: ISession }) {
  return (
        <div className='user-container'>
            <img src="https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg" className='user-img'/>
            { user.user ? (user.user.username || 'Log In') : 'Log In' }
        </div>
  );
}
