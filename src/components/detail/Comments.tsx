import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  FormEvent
} from 'react';
import { throttle } from 'lodash';
import { fetchComments, postComments } from '../../API/Fetch';
import { SERVER_ERROR_MSG } from '../../adapters/globals';
import Pagination from '../videogames/Pagination';
import Comment from './Comment';
import { ISession } from '../../interfaces/User';
import { IComment } from '../../interfaces/Videogames';

export default function Comments({ id, user }: { id: string, user: ISession}) {
  const [allComments, setAllComments] = useState<IComment[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [error, setError] = useState('');
  const [input, setInput] = useState('');
  const commentsPerPage = 6;
  const errorContent = useRef<any>();

  useEffect(() => {
    fetchComments(id)
      .then((data) => {
        setAllComments(data.reverse());
      })
      .catch(() => alert(SERVER_ERROR_MSG));
  }, []);

  const paginate = (page: number) => {
    setCurrentPage(page);
  };

  const indexOfLastComment = currentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  const currentComment = allComments.slice(indexOfFirstComment, indexOfLastComment);

  const handleSubmit = () => {
    if (input.trim() === '') {
      setError('Your comment cannot be empty');
      errorContent.current.style.display = 'block';
      return;
    }
    if (user.user) {
      postComments(input, user.jwt, id)
        .then((data) => {
          const comment = {
            comment: data.body,
            id: data.id,
            user: data.user.username
          };
          setAllComments([comment, ...allComments]);
          setInput('');
          setError('');
          errorContent.current.style.display = 'none';
        })
        .catch(() => {
          setError('Something went wrong, try later');
          errorContent.current.style.display = 'block';
        });
    }
  };

  const throttledFunction = useCallback(throttle(() => handleSubmit(), 2000), [input]);

  const throttleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    throttledFunction();
  };

  return (
    <div className='comments-container'>
      {user && user.user ? (
        <>
        <div ref={errorContent} className='comments-form-error'>{error}</div>
        <form onSubmit={throttleSubmit} className='comments-form'>
          <textarea onChange={(event) => setInput(event.target.value)} value={input} name='body' placeholder='Send a comment!' />
          <input className='comments-form-submit' type='submit' value='Send' />
        </form>
        </>
      ) : (
        <div className='comments-form'>If you want to leave a comment, please log in.</div>
      )}
      {currentComment && currentComment.length > 0 ? (
        currentComment.map(
          ({ id, comment, user }) => comment !== '' && <Comment key={id} user={user} comment={comment}></Comment>
        )
      ) : (
        <div className='comment'>No comments yet. Be the first one!</div>
      )}
      <Pagination
        itemsPerPage={commentsPerPage}
        totalItems={allComments.length}
        paginate={paginate}
        currentPage={currentPage}
        noScroll={true}
      />
    </div>
  );
}
