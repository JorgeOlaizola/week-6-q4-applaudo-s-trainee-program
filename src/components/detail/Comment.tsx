import React, { memo } from 'react';
import { IComment } from '../../interfaces/Videogames';

function Comment({ comment, user }: IComment) {
  return (
    <div className='comment'>
      <div className='comment-user'>{user}</div>
      <div className='comment-body'>{comment}</div>
    </div>
  );
}

export default memo(Comment);
