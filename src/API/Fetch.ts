import { BASE_URL } from '../adapters/globals';
import { get, post } from './FetchInfo';
import {
  IVideogame, IPlatform, IVideogameDetail, IComment
} from '../interfaces/Videogames';
import { AuthorizationHeader } from '../adapters/utils';

export async function fetchVideogames(query?: string) {
  const videogames: any[] = await get(`${BASE_URL}/games${query ? `?name_contains=${query}` : ''}`);
  const response: IVideogame[] = videogames.map(({ name, id, cover_art }) => ({
    name,
    id,
    image: cover_art && cover_art.formats.small.url,
  }));
  return response;
}

export async function fetchVideogameDetail(gameId: number | string) {
  const videogames: any = await get(`${BASE_URL}/games/${gameId}`);
  const {
    name, cover_art, release_year, genre, id, platforms, price
  } = videogames;
  const response: IVideogameDetail = {
    name,
    image: cover_art && cover_art.formats.small.url,
    releaseYear: release_year,
    genre: {
      id: genre.id,
      name: genre.name,
    },
    platforms: platforms.map(({ name, id }: IPlatform) => ({ name, id })),
    price,
    id
  };
  return response;
}

export async function fetchComments(gameId: number | string) {
  const comments: any[] = await get(`${BASE_URL}/games/${gameId}/comments`);
  const response: IComment[] = comments.map(({
    body, id, user
  }) => ({
    comment: body,
    user: user.username,
    id
  }));
  return response;
}

export async function postComments(body: string, token: string, gameId: number | string) {
  return post<any, any>(`${BASE_URL}/games/${gameId}/comment`, { body }, { headers: AuthorizationHeader(token) });
}
