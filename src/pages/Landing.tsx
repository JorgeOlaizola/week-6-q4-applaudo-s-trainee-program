import React from 'react';
import { Link } from 'react-router-dom';
import { PAGE_ICON } from '../adapters/globals';
import '../styles/landing.scss';

export default function Landing() {
  return (
    <div className='landing-container'>
      <div className='landing-content'>
        <h1 className='landing-title'>Welcome to the videogames App</h1>
        <div className='landing-info-container'>
          <img className='landing-logo' src={PAGE_ICON} alt='logo-image' />
          <div className='landing-info'>
            This app is developed in TypeScript and it allows you to navigate through different
            games, authenticate yourself and leave comments. Make sure to follow me on GitLab!
          </div>
        </div>
          <Link className='landing-link landing-button' to='/videogames'>
            Get started!
          </Link>
      </div>
    </div>
  );
}
