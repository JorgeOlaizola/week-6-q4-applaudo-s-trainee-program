/* eslint-disable no-unused-vars */
import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Redirect, useHistory } from 'react-router';
import { logIn } from '../adapters/authentication';
import { AuthorizationHeader, RegularHeader } from '../adapters/utils';
import { Routes } from '../interfaces/Enum';
import { ISession } from '../interfaces/User';
import '../styles/user.scss';

/* eslint no-undef: 0 */
export default function LogIn({
  isAuthenticated,
  useSession,
  useListener,
}: {
  isAuthenticated: () => boolean;
  useSession: (session: ISession | {}) => void;
  useListener: () => void;
}) {
  const [form, setForm] = useState({ identifier: '', password: '' });
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const responseHandler = (data: any) => {
    console.log(data);
    if (data.statusCode) return setError('Invalid username or password');
    useSession(data);
    useListener();
    history.push('/videogames');
    return undefined;
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (error !== '') setError('');
    setLoading(true);
    logIn(form, RegularHeader())
      .then((data) => {
        setLoading(false);
        responseHandler(data);
      })
      .catch((e) => {
        setLoading(false);
        setError('Something went wrong');
      });
  };

  return !isAuthenticated() ? (
    <div className='login-container'>
      {loading && <div className='login-loading'>Sending request...</div>}
      {error && <div className='login-error'>{error}</div>}
      <h1 className='login-title'>Log In</h1>
      <form onSubmit={handleSubmit} className='login-form'>
        <input
          onChange={handleChange}
          className='login-form-input'
          type='text'
          name='identifier'
          placeholder='Username'
        ></input>
        <input
          onChange={handleChange}
          className='login-form-input'
          type='password'
          name='password'
          placeholder='Password'
        ></input>
        <input className='login-form-submit' type='submit' value='Log In'></input>
      </form>
    </div>
  ) : (
    <Redirect to={{ pathname: Routes.profile }} />
  );
}
