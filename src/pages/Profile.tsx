/* eslint-disable no-unused-vars */
import React from 'react';
import { Redirect } from 'react-router';
import { Routes } from '../interfaces/Enum';
import { ISession } from '../interfaces/User';

export default function Profile({
  useSession, useListener, session, isAuthenticated
}: {
    useSession: (session: ISession | {}) => void,
    useListener: () => void,
    session: ISession,
    isAuthenticated: () => boolean
}) {
  return (
    isAuthenticated() ?
    <div className='login-container'>
      <h1 className='login-title'>Account</h1>
      <img
        className='user-img-big'
        src='https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg'
      />
      <span className='login-user-item'>Firstname: {session.user.firstName}</span>
      <span className='login-user-item'>Lastname: {session.user.lastName}</span>
      <span className='login-user-item'>Email: {session.user.email}</span>
      <span className='login-user-item'>Username: {session.user.username}</span>
      <span className='login-user-item'>User ID: {session.user.id}</span>
      <button
        className='login-button-logout'
        onClick={() => {
          useSession({});
          useListener();
        }}
      >
        Log Out
      </button>
    </div> :
    <Redirect to={{ pathname: Routes.login }} />
  );
}
