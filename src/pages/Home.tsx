import React, { useEffect, useReducer } from 'react';
import Pagination from '../components/videogames/Pagination';
import Videogames from '../components/videogames/Videogames';
import '../styles/home.scss';
import {
  INIT_FETCH, FINISH_FETCH, PAGINATE, UPDATE
} from '../reducers/actionTypes';
import { fetchVideogames } from '../API/Fetch';
import SearchVideogames from '../components/videogames/SearchVideogames';
import { initialState, reducer } from '../reducers';

export default function Home({ listener }: { listener: boolean }) {
  const videogamesPerPage = 8;
  const [state, dispatch] = useReducer(reducer, initialState);

  const getQueryParams = () => {
    if (!window.location.search) return;
    const queryParams = window.location.search.split('?')[1].split('&');
    let queryString: string = '';
    queryParams.forEach((query) => {
      const match = query.split('=');
      if (match[0].toLowerCase() === 'search') queryString = match[1].toLowerCase();
      // eslint-disable-next-line radix
      if (match[0] === 'page') dispatch({ type: PAGINATE, payload: parseInt(match[1]) });
    });
    // eslint-disable-next-line consistent-return
    return queryString;
  };

  useEffect(() => {
    let mounted = true;
    dispatch({ type: INIT_FETCH });
    fetchVideogames(getQueryParams()).then((videogames) => {
      if (mounted) {
        dispatch({ type: FINISH_FETCH, payload: videogames });
      }
    });
    return () => {
      mounted = false;
    };
  }, [state.interaction, listener]);

  const paginate = (page: number) => {
    dispatch({ type: PAGINATE, payload: page });
  };

  const interactionSetter = (query: string): void => {
    dispatch({ type: UPDATE, payload: query });
  };

  const indexOfLastVideogame = state.currentPage * videogamesPerPage;
  const indexOfFirstVideogame = indexOfLastVideogame - videogamesPerPage;
  const currentVideogame = state.videogames.slice(indexOfFirstVideogame, indexOfLastVideogame);
  return (
    <div className='home'>
      <SearchVideogames interactionSetter={interactionSetter} />
      <Videogames videogames={currentVideogame} loading={state.loading} />
      <Pagination
        itemsPerPage={videogamesPerPage}
        totalItems={state.videogames.length}
        paginate={paginate}
        currentPage={state.currentPage}
        noScroll={false}
      ></Pagination>
    </div>
  );
}
