import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import DetailInfo from '../components/detail/DetailInfo';
import Comments from '../components/detail/Comments';
import { SERVER_ERROR_MSG } from '../adapters/globals';
import { fetchVideogameDetail } from '../API/Fetch';
import '../styles/detail.scss';
import { ISession } from '../interfaces/User';

export default function Detail({
  id, user
}: {
  id: string, user: ISession
}) {
  const [game, setGame]: [any, any] = useState({});
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  useEffect(() => {
    setLoading(true);
    fetchVideogameDetail(id)
      .then((videogame) => {
        setGame(videogame);
        setLoading(false);
      })
      .catch(() => alert(SERVER_ERROR_MSG));
  }, [id]);

  const {
    name,
    image,
    genre,
    platforms,
    releaseYear,
    price
  } = game;

  return (
    <div className='detail-comments-container'>
      {loading ? (
        <div className='detail-container'>Loading</div>
      ) : (
        <div className='detail-container'>
          <button className='detail-back-button' onClick={() => history.push('/videogames')}>
            ⬅ Go Back
          </button>
          {game && <DetailInfo
            name={name}
            image={image}
            genre={genre}
            platforms={platforms}
            releaseYear={releaseYear}
            price={price}
            id={id}
          />}
        </div>
      )}
      <Comments id={id} user={user} />
    </div>
  );
}
