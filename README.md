# Videogames App - Week 6 Q4 Applaudo Trainee Program

Application that uses the Trainee Gamerbox api (https://trainee-gamerbox.herokuapp.com) to fetch videogames. Coded in TypeScript.

## Try it

Link to deploy: https://week-6-q4-applaudo-s-trainee-program.vercel.app/

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/week-6-q4-applaudo-s-trainee-program.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

Execute the following command to run the project in http://localhost:3000

```bash
  npm start
```
